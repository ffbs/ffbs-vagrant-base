#!/bin/sh -eux

echo "update base system"
apt-get update
apt-get --no-install-recommends -qq dist-upgrade

echo "configure ssh key autogeneration"
mkdir -p /etc/systemd/system/ssh.service.d/
cat > /etc/systemd/system/ssh.service.d/ssh-keygen.conf <<EOF
[Service]
ExecStartPre=/usr/bin/ssh-keygen -A
EOF

echo "install packages from backports"
cat /etc/apt/sources.list
cat > /etc/apt/sources.list <<EOF
deb http://deb.debian.org/debian stretch main
deb http://deb.debian.org/debian stretch-updates main
deb http://deb.debian.org/debian stretch-backports main
deb http://security.debian.org/ stretch/updates main
EOF
apt-get -qy update
apt-get -qy -t stretch-backports --no-install-recommends install linux-image-amd64 linux-headers-amd64 iproute2

echo "cleanup"
apt-get clean

echo "reboot"
systemd-run --on-active=3 --timer-property=AccuracySec=1s systemctl reboot
