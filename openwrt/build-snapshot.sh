#!/bin/bash -xeu

if [ ! -e snapshot-x86-64-combined-ext4.img.gz ]; then
  wget https://downloads.openwrt.org/snapshots/targets/x86/64/openwrt-x86-64-combined-ext4.img.gz -O snapshot-x86-64-combined-ext4.img.gz
fi

zcat snapshot-x86-64-combined-ext4.img.gz > snapshot-x86-64-combined-ext4.img

export PACKER_KEY_INTERVAL=10ms
packer build openwrt-snapshot.json
