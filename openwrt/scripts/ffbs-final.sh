#!/bin/sh -eux

echo "remove ssh host keys"
rm /etc/dropbear/*_host_key

echo "fill rootfs with zeros"
cat /dev/zero > /ZERO || true
rm /ZERO
sync
