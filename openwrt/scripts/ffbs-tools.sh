#!/bin/sh -eux

echo "install tools"
opkg update
opkg install bash openssh-sftp-server python rsync sudo luci
opkg install wireguard

echo "configure ssh key"
mkdir -p /root/.ssh
chmod 0700 /root/.ssh
cat > /root/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key
EOF
chmod 0600 /root/.ssh/authorized_keys
ln -s /root/.ssh/authorized_keys /etc/dropbear
uci set dropbear.@dropbear[0].PasswordAuth='off'
uci commit

echo "set root password"
lua - <<EOF
local sys = require "luci.sys"
sys.user.setpasswd("root", "root")
EOF

echo "reduce grub timeout"
mount -o remount,rw /boot
sed -i 's/timeout="5"/timeout="1"/' /boot/grub/grub.cfg

echo "fill bootfs with zeros"
cat /dev/zero > /mnt/ZERO || true
rm /mnt/ZERO
sync
